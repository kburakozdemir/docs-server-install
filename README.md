# Server Installation

This repo contains documentation via `mkdocs` for LAMP installation on Ubuntu 18.04 server with advanced features.

> Please keep in mind that this is a work in progress!
> This must be moved to binbiriz main docs repo!

## Pipeline Status

This repo is automatically deployed to GitLab pages via CI/CD.

[![pipeline status](https://gitlab.com/kburakozdemir/docs-server-install/badges/master/pipeline.svg)](https://gitlab.com/kburakozdemir/docs-server-install/commits/master)

## Development

Documentation development depends on [MkDocs](https://www.mkdocs.org/) and [Material for MkDocs](https://github.com/squidfunk/mkdocs-material).

To get started see [MkDocs Documentation](https://www.mkdocs.org/#getting-started).

Documentation files are in `docs` folder and configuration file is `mkdocs.yml`. For faster development [squidfunk/mkdocs-material](https://hub.docker.com/r/squidfunk/mkdocs-material) Docker image is used.

For development, simply run the following command in the repo root and visit [http://0.0.0.0:8900/](http://0.0.0.0:8900/):

```bash
docker image rm squidfunk/mkdocs-material-pluggedin:latest
docker build -t squidfunk/mkdocs-material-pluggedin  - < ./mkdocs_Dockerfile
docker run --rm -it \
  -p 8900:8000 -v ${PWD}:/docs \
  squidfunk/mkdocs-material-pluggedin:latest
```

## Old URL

See this [GitLab Pages URL](https://binbiriz.gitlab.io/docs/docs-server-install/).

## New URL

See this [GitLab Pages URL](https://kburakozdemir.gitlab.io/docs-server-install/).

## Credits and Attribution

Icon used for this repo is made by [Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com/).
