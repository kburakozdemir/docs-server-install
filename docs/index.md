---
title: Inital Installations for LAMP Server | Binbiriz
description: Inital Installations for LAMP server.
---

# Install a LAMP Server on Ubuntu 18.04 LTS

These are **initial installation** steps needed.

> Do these step by step. Be careful! Be patient!

## FQDN & Hostname

Be sure that you have FQDN and a proper hostname. If needed, configure your DNS and reverse DNS records.

It is better to create an A DNS record for your hostname.

## Update Server

### Update Script

First of all, update the server!

Install this [snippet](https://gitlab.com/-/snippets/1840894) for root user.

```bash
# Run updates
sh ./scripts/update_packages.sh
```

### sudo user

#### Create

> Prepare a sudo password

```bash
adduser [username]
usermod -aG sudo [username]
id [username]
```

#### Sync same pub key of root to user

```bash
# One-liner
# rsync same pub key of root to user [username]
rsync --archive \
  --chown=username:username \
  ~/.ssh /home/username
```

### ufw Firewall

```bash
sudo ufw allow ssh
sudo ufw allow http
sudo ufw allow https
sudo ufw status
sudo ufw enable
```

## Install Essential Packages

```bash
# Install essential packages
sudo apt install landscape-common snapd \
  software-properties-common \
  unrar-free p7zip-full p7zip-rar \
  unzip zip curl htop git webp -y

# Restart the server
sudo reboot -h now
```

## Server Notifier Script

### Reboot Notifier

Refer to this [gist](https://gist.github.com/kburakozdemir/d10b456fcf94d34d2c753b25bf9bc42f).

```bash
# To enable for all users
sudo nano /etc/profile.d/notice.sh
```

<script src="https://gist.github.com/kburakozdemir/d10b456fcf94d34d2c753b25bf9bc42f.js"></script>

## Canonical LivePatch

```bash
# Install Canonical LivePatch
sudo snap install canonical-livepatch

# Restart the server
sudo reboot -h now
```

You can obtain API key from [here](https://ubuntu.com/security/livepatch).

```bash
# Enable Canonical LivePatch
sudo canonical-livepatch enable [api_key_optained_from_official_page]

# Restart the server
sudo reboot -h now
```

```bash
# Check the status
sudo canonical-livepatch status
```

## Server Time & ntp

Timezone for Turkey is `Europe/Istanbul`.

[How To Set Up Time Synchronization on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-time-synchronization-on-ubuntu-18-04)
