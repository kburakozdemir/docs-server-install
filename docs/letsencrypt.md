# Let's Encrypt Installation

Free SSL for the masses! [Let's Encrypt](https://letsencrypt.org/)!

## Certificate Renewal

As of the time this page has been edited, there is no need in Ubuntu 18.04 to setup a crontab job for certificate renewal. A system cronjob is added during `certbot` installation.

## Installation

```bash
sudo apt install certbot python-certbot-apache -y
```

## Obtaining The Certificate

> Before trying the following, make sure that site is enabled and working.

### Apache Method

```bash
# Subdomain
sudo certbot -m your-email@address.com \
  --apache --agree-tos \
  -d subdomain.domain.com

# or

## TLD
sudo certbot -m your-email@address.com \
  --apache --agree-tos \
  -d domain.com \
  -d www.domain.com
```

### Webroot Method

#### Direct

```bash
## Subdomain
sudo certbot run \
  -m your-email@address.com \
  -a webroot \
  -i apache \
  -w /var/www/path/to/document/root \
  -d subdomain.domain.com

# or

# TLD
sudo certbot run \
  -m your-email@address.com \
  -a webroot \
  -i apache \
  -w /var/www/path/to/document/root \
  -d domain.com \
  -d www.domain.com
```

#### certonly

```bash
# Subdomain
sudo certbot -m your-email@address.com \
  --agree-tos -n certonly \
  --webroot -w /var/www/path/to/document/root \
  -d subdomain.domain.com

# or

## TLD
sudo certbot -m your-email@address.com \
  --agree-tos -n certonly \
  --webroot -w /var/www/path/to/document/root \
  -d domain.com \
  -d www.domain.com
```
