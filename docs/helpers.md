# Helpers

## Development Folder Structures

### Drupal

> This is a composer based Drupal development folder structure for Lando, however it can be implemented to any other Drupal project.
> This structure ensures keeping your development directory very tidy!
> This structure is inspired from the beautiful [Drupal VM](https://www.drupalvm.com/) project.

#### Repository Root Directory

##### Directories

```txt
burak@kbo-thinkpad-x1-carbon  ~/works/lando  tree ./dev-project -L 1 -d -a --dirsfirst
./dev-project
├── drupal
├── .git
├── scripts
└── .vscode

4 directories
```

##### Files & Directories

```txt
burak@kbo-thinkpad-x1-carbon  ~/works/lando  tree ./dev-project -L 1 -a --dirsfirst
./dev-project
├── drupal
├── .git
├── scripts
├── .vscode
├── .gitignore
├── .gitlabci-rsync-exclude.txt
├── .gitlab-ci.yml
├── .lando.yml
└── README.md

4 directories, 5 files
```

```txt
burak@kbo-thinkpad-x1-carbon  ~/works/lando  tree ./dev-project/scripts -L 2 -a --dirsfirst
./dev-project/scripts
├── lando-customizations
│   ├── php.ini
│   └── varnish.vcl
├── contentpull.sh
├── dbpull.sh
├── devs.sh
├── full_dev.sh
├── update.sql
├── xxxpulldbfromprod.sh
└── xxxpulldbfromtest.sh

1 directory, 9 files
```

```txt
burak@kbo-thinkpad-x1-carbon  ~/works/lando  tree ./dev-project/drupal -L 3 -a --dirsfirst
./dev-project/drupal
├── config
│   └── sync
├── drush
│   ├── Commands
│   │   └── PolicyCommands.php
│   ├── sites
│   │   └── self.site.yml
│   ├── drush.yml
│   ├── README.md
│   └── stgm.aliases.drushrc.php
├── private-files
│   ├── .gitkeep
│   └── .htaccess
├── scripts
│   ├── composer
│   │   └── ScriptHandler.php
│   ├── permissions-dev.sh
│   ├── permissions-prod.sh
│   ├── permissions-test.sh
│   ├── post_deploy_dev.sh
│   ├── post_deploy_prod.sh
│   ├── post_deploy_test.sh
│   ├── to-dev-from-test.sh
│   ├── to-test-from-prod.sh
│   └── update.sql
├── web
│   ├── modules
│   │   ├── custom
│   │   └── .gitignore
│   ├── profiles
│   │   └── .gitignore
│   ├── sites
│   │   ├── default
│   │   │   ├── .gitignore
│   │   │   └── services.yml
│   │   ├── dev.services.yml
│   │   └── .gitignore
│   ├── themes
│   │   ├── custom
│   │   └── .gitignore
│   └── .gitignore
├── composer.json
├── composer.lock
├── .env.example
├── .gitignore
├── LICENSE
├── load.environment.php
├── phpunit.xml.dist
├── README.md
└── .travis.yml
```

## Listing Enabled Apache Modules

```bash
sudo apache2ctl -M
```

## Quick Drupal Installation

```bash
composer create-project drupal/recommended-project:^8 drupal

# One-liner
drush site-install standard \
  --locale=en \
  --db-url=mysql://dbuser:"password"@host:port/dbname \
  --site-name=Drupal_8_Quick_Install \
  --account-name=admin --account-pass=admin --yes
```

## Test Headers

### Using curl

```bash
curl -s -D - "https://www.domain.com/page" -o /dev/null
```

```bash
# One-liner
curl --user authuser:authpassword \
  -s -D - "https://subdomain.domain.com/a/page" \
  -o /dev/null

curl --user authuser:authpassword -I https://subdomain.domain.com/
```

### Dries Buytaert

[HTTP Headers Analyzer](https://dri.es/headers)

## Follow Redirection Sequence

Refer to this [issue](https://gitlab.com/kburakozdemir/httpsnonwww/-/issues/2).

## Copy files

```bash
chown -R username:www-data /var/www/

# One-liner
rsync -avzth --progress --delete \
  username@source.hostname-or-ip:/var/www/vhosts/domain.com/test-project.domain.com/drupal/ \
  /var/www/vhosts/domain.com/prod-project.domain.com/drupal/
# as root
bash /var/www/vhosts/domain.com/prod-project.domain.com/drupal/scripts/permissions-prod.sh
```

## Misc

```bash
# helpers
cp /etc/varnish/default.vcl /etc/varnish/default.vcl.back
curl https://gitlab.agile.coop/-/snippets/27/raw -o /etc/varnish/default.vcl
```

```bash
cp -r /root/.ssh /home/binbiriz/.ssh
chown -R binbiriz: /home/binbiriz/.ssh
# ftp binbiriz ssh folder
```

```bash
# create web folders
mkdir -p /var/www/vhosts/domain.com/prod-project.domain.com/drupal/web
mkdir -p /var/www/vhosts/domain.com/prod-project.domain.com/logs
echo "test file" > /var/www/vhosts/domain.com/prod-project.domain.com/drupal/web/index.html
```

## Crontab

```bash
5 * * * * drush --root=/var/www/path/to/drupal/document/root/ cron
```

## Flush DNS Cache

```bash
sudo systemctl restart systemd-resolved
sudo systemd-resolve --statistics
```
