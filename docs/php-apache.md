# PHP & Apache

## In the Beginning

> Do everything in the following order

1. Install mod_php
2. Add your user to www-data group
3. Install php_fpm

## mod_php

Add php repository

```bash
sudo add-apt-repository ppa:ondrej/php
sudo apt update
```

```bash
# php 7.4
sudo apt-get install -y \
  apache2 apache2-utils \
  php7.4 php7.4-cli php7.4-common php7.4-mbstring \
  php7.4-zip php7.4-xml php7.4-curl php7.4-bcmath \
  php7.4-json php7.4-gd php7.4-mysql php7.4-opcache \
  php7.4-xml libapache2-mod-php7.4 php7.4-apcu php7.4-pcov \
  php-uploadprogress

# for Moodle
sudo apt install php7.4-intl php7.4-tokenizer php7.4-soap php7.4-xmlrpc

sudo a2enmod http2 expires headers deflate rewrite remoteip php7.4

# php 8.1
sudo apt-get install -y \
  apache2 apache2-utils \
  php8.1 php8.1-cli php8.1-common php8.1-mbstring \
  php8.1-zip php8.1-xml php8.1-curl php8.1-bcmath \
  php8.1-gd php8.1-mysql php8.1-opcache \
  php8.1-xml libapache2-mod-php8.1 php8.1-apcu php8.1-pcov \
  php8.1-uploadprogress

# for Moodle
sudo apt install php8.1-intl php8.1-tokenizer php8.1-soap php8.1-xmlrpc

sudo a2enmod http2 expires headers deflate rewrite remoteip php8.1

# Check whether there is only one php
sudo update-alternatives --config php
```

## Add user to www-data group

```bash
sudo usermod -aG www-data [username]
id  [username]

# Logout and login again before preceeding
```

```bash
cd /var/www
sudo chown -R binbiriz:www-data html
sudo chown -R binbiriz:www-data html/
```

## php_fpm

> HTTP2 cannot be achieved without php-fpm.

[How to enable HTTP/2 support in Apache](https://http2.pro/doc/Apache).

### php 7.4

```bash
# Install the php-fpm from your PHP repository.
# This package name depends on the vendor.
sudo apt-get install php7.4-fpm
sudo a2enmod proxy_fcgi setenvif

# Again, this depends on your PHP vendor.
sudo a2enconf php7.4-fpm

# This disables mod_php.
sudo a2dismod php7.4

# This disables the prefork MPM.
# Only one MPM can run at a time.
sudo a2dismod mpm_prefork

# Enable event MPM.
# You could also enable mpm_worker.
sudo a2enmod mpm_event

# Restart apache
sudo systemctl restart apache2
```

### php 8.1

```bash
# Install the php-fpm from your PHP repository.
# This package name depends on the vendor.
sudo apt-get install php8.1-fpm
sudo a2enmod proxy_fcgi setenvif

# Again, this depends on your PHP vendor.
sudo a2enconf php8.1-fpm

# This disables mod_php.
sudo a2dismod php8.1

# This disables the prefork MPM.
# Only one MPM can run at a time.
sudo a2dismod mpm_prefork

# Enable event MPM.
# You could also enable mpm_worker.
sudo a2enmod mpm_event

# Restart apache
sudo systemctl restart apache2
```

## php.ini Config

`nano /etc/php/7.4/fpm/php.ini`

```txt
memory_limit = 256M

max_input_vars = 5000

; Maximum allowed size for uploaded files.
upload_max_filesize = 100M

; The maximum size in bytes of data that
; can be posted with the POST method.
; Typically, should be larger
; than upload_max_filesize and
; smaller than memory_limit.
post_max_size = 128M
```

References:

1. https://stackoverflow.com/questions/2184513/change-the-maximum-upload-file-size
2. https://medium.com/@themehigh/what-is-max-input-vars-and-how-to-change-it-d95d576cb0c6

## Apache Security

```bash
nano /etc/apache2/conf-enabled/security.conf
```

> Find these lines in the file and edit

```txt
ServerSignature Off
ServerTokens Prod
```

## HTTP2

```bash
sudo nano /etc/apache2/apache2.conf
```

> Add this lines to apache2.conf file (just after log formats)

```txt
Protocols h2 http/1.1
Protocols h2 h2c http/1.1
```

```bash
sudo systemctl restart apache2
```

## Logging Real Visitor IPs (for Cloudflate)

> Never install mod_cloudflare! **Use remoteip instead**.

[Restoring original visitor IPs - Option 2: Installing mod_remoteip with Apache](https://support.cloudflare.com/hc/en-us/articles/360029696071-Restoring-original-visitor-IPs-Option-2-Installing-mod-remoteip-with-Apache)

sudo tail -f /var/log/apache2/access.log

```sh
<VirtualHost *:80>
ServerAdmin webmaster@localhost
DocumentRoot /var/www/html
ServerName remoteip.andy.support
RemoteIPHeader CF-Connecting-IP
ErrorLog ${APACHE_LOG_DIR}/error.log
CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
```

sudo nano /etc/apache2/sites-enabled/000-default.conf
sudo systemctl restart apache2

sudo apachectl configtest

change log format:
sudo nano /etc/apache2/apache2.conf

new log format instead of LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined

LogFormat "%a %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined

sudo nano /etc/apache2/conf-available/remoteip.conf

## Mime Types Apache

Reference: [MIME types (IANA media types)](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types)

```bash
nano /etc/mime.types
```

Add mime type webp.

```txt
image/webp  webp
```
