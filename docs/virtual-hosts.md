# Virtual Hosts

## Apache

### HTTP Basic Authentication

[How To Set Up Password Authentication with Apache on Ubuntu 18.04 [Quickstart]](https://www.digitalocean.com/community/tutorials/how-to-set-up-password-authentication-with-apache-on-ubuntu-18-04-quickstart)

### Setup Default Virtual Hosts

```sh
cd /etc/apache2/sites-available
sudo a2dissite 000-default.conf
sudo rm 000-default.conf
sudo wget https://gist.githubusercontent.com/kburakozdemir/ec83961bc69968aaaa5edfdf5d387585/raw/fc9c5af8fb1f5f5a9b815a4839355a939b036e98/example.IP.000-default.conf -O 000-default.conf
sudo nano 000-default.conf
sudo a2ensite 000-default.conf

# Backup org index.html and put a new one

sudo mv "/var/www/html/index.html" "/var/www/index.html.back"

sudo wget https://gist.githubusercontent.com/kburakozdemir/ec83961bc69968aaaa5edfdf5d387585/raw/6bfb7685122070aa17ec29a7e6b200aedf73066e/index.default.html -O /var/www/html/index.html

sudo wget https://binbiriz.com/sites/default/files/binbirizfootprint1.png -O /var/www/html/favicon.png

cd /var/www
sudo chown -R www-data: html/
sudo service apache2 restart
```

### Apache Virtual Host

Create `vhosts` folder

```sh
sudo mkdir -p /var/www/vhosts
cd /var/www
sudo chown -R www-data: vhosts/
sudo service apache2 restart
```

Create a virtual hosts owner

```sh
sudo mkdir -p /var/www/vhosts/binbiriz.com
sudo chown -R www-data: /var/www/vhosts/binbiriz.com
sudo chown -R www-data: /var/www/vhosts/binbiriz.com/
sudo service apache2 restart
```

Create a virtual host

```sh
sudo mkdir -p /var/www/vhosts/binbiriz.com/subdomain.binbiriz.com
sudo chown -R www-data: /var/www/vhosts/binbiriz.com/subdomain.binbiriz.com
sudo chown -R www-data: /var/www/vhosts/binbiriz.com/subdomain.binbiriz.com/
sudo service apache2 restart

sudo wget https://gist.githubusercontent.com/kburakozdemir/ec83961bc69968aaaa5edfdf5d387585/raw/6bfb7685122070aa17ec29a7e6b200aedf73066e/index.html -O /var/www/vhosts/binbiriz.com/subdomain.binbiriz.com/index.html
sudo wget https://binbiriz.com/sites/default/files/binbirizfootprint1.png -O /var/www/vhosts/binbiriz.com/subdomain.binbiriz.com/favicon.png

sudo chown -R www-data: /var/www/vhosts/binbiriz.com/subdomain.binbiriz.com/
```

```sh
# One-liner
sudo wget https://gist.githubusercontent.com/kburakozdemir/ec83961bc69968aaaa5edfdf5d387585/raw/53cd5f21672e43f1c0ba6d4181af5546e43d3af2/example.template.conf \
  -O /etc/apache2/sites-available/subdomain.binbiriz.com.conf

sudo nano /etc/apache2/sites-available/subdomain.binbiriz.com.conf

sudo systemctl restart apache2

cd /etc/apache2/sites-available
sudo a2ensite subdomain.binbiriz.com.conf
sudo systemctl restart apache2
```

Burada önemli olan husus en son ssl için Let's Encrypt'in oluşturduğu VirtualHost konfigürasyon dosyasının sonunda non-www yönlendirmesi yapılması.

A

```conf
<VirtualHost *:80>
  ServerName domain.com.tr
  ServerAlias www.domain.com.tr

  ServerAdmin webmaster@localhost

  RemoteIPHeader CF-Connecting-IP

  DocumentRoot /var/www/vhosts/domain.com.tr/drupal/web

  <Directory /var/www/vhosts/domain.com.tr/drupal/web>
    Options -Indexes +FollowSymLinks +MultiViews
    AllowOverride All
    Require all granted
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

  "ProxyPassMatch ^/(.*\.php(/.*)?)$unix:/run/php/php7.2-fpm.sock|fcgi://localhost/var/www/vhosts/domain.com.tr/drupal/web/"
</VirtualHost>
```

B

```conf
<VirtualHost *:80>
  ServerName domain.com.tr
  ServerAlias www.domain.com.tr

  ServerAdmin webmaster@localhost

  RemoteIPHeader CF-Connecting-IP

  DocumentRoot /var/www/vhosts/domain.com.tr/drupal/web

  <Directory /var/www/vhosts/domain.com.tr/drupal/web>
    Options -Indexes +FollowSymLinks +MultiViews
    AllowOverride All
    Require all granted
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

  ProxyPassMatch ^/(.*\.php(/.*)?)$ unix:/run/php/php7.2-fpm.sock|fcgi://localhost/var/www/vhosts/domain.com.tr/drupal/web/

  RewriteEngine on
  RewriteCond %{SERVER_NAME} =www.domain.com.tr [OR]
  RewriteCond %{SERVER_NAME} =domain.com.tr
  RewriteRule ^ https://%{SERVER_NAME}%{REQUEST_URI} [END,NE,R=permanent]
</VirtualHost>
```

After Let's Encrypt

```config
<IfModule mod_ssl.c>
<VirtualHost *:443>
  ServerName domain.com.tr
  ServerAlias www.domain.com.tr

  ServerAdmin webmaster@localhost

  RemoteIPHeader CF-Connecting-IP

  DocumentRoot /var/www/vhosts/domain.com.tr/drupal/web

  <Directory /var/www/vhosts/domain.com.tr/drupal/web>
    Options -Indexes +FollowSymLinks +MultiViews
    AllowOverride All
    Require all granted
  </Directory>

  ErrorLog ${APACHE_LOG_DIR}/error.log
  CustomLog ${APACHE_LOG_DIR}/access.log combined

  ProxyPassMatch ^/(.*\.php(/.*)?)$ unix:/var/run/php/php7.2-fpm.sock|fcgi://localhost/var/www/vhosts/domain.com.tr/drupal/web/

  Include /etc/letsencrypt/options-ssl-apache.conf
  SSLCertificateFile /etc/letsencrypt/live/domain.com.tr/fullchain.pem
  SSLCertificateKeyFile /etc/letsencrypt/live/domain.com.tr/privkey.pem

  RewriteEngine on
  RewriteCond %{HTTP_HOST} ^www\.(.+) [NC]
  RewriteRule ^ https://%1%{REQUEST_URI} [L,R=301]
</VirtualHost>
</IfModule>
```
