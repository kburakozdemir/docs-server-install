# Cache

## Varnish

```bash
sudo systemctl daemon-reload && sudo systemctl restart varnish.service
```

**REAL IP:**

[HOWTO: Log Client IP AND X-Forwarded-For IP in Apache](https://www.techstacks.com/howto/log-client-ip-and-xforwardedfor-ip-in-apache.html)

mod_remoteip'ye gerek var mı kurulu olmasına emin değilim.

`/etc/apache2/apache2.conf`

```conf
# Note that the use of %{X-Forwarded-For}i instead of %h is not recommended.
# Use mod_remoteip instead.
#
LogFormat "%v:%p %h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" vhost_combined
#LogFormat "%h %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
#LogFormat "%a %l %u %t \"%r\" %>s %O \"%{Referer}i\" \"%{User-Agent}i\"" combined
# apache varnish nginx REAL visitor IP
LogFormat "%{X-Forwarded-For}i %l %u %t \"%r\" %>s %b \"%{User-agent}i\"" combined
LogFormat "%h %l %u %t \"%r\" %>s %O" common
LogFormat "%{Referer}i -> %U" referer
LogFormat "%{User-agent}i" agent
```

## Varnish SSL Termination via Apache (Apache-Varnish-Apache)

### References

[How To Use Apache2 for SSL Termination With Varnish](https://bash-prompt.net/guides/apache-varnish/)

[Apache SSL Termination (HTTPS Varnish cache)](https://medium.com/@sivaschenko/apache-ssl-termination-https-varnish-cache-3940840531ad)

[Purpose of the X-Forwarded-Proto HTTP Header](https://community.pivotal.io/s/article/Purpose-of-the-X-Forwarded-Proto-HTTP-Header?language=en_US)

[Varnish and HTTPS with Apache](http://davidbu.ch/mann/blog/2015-03-20/varnish-and-https-apache.html)

[Redirect HTTP to HTTPS using Varnish](https://www.rsreese.com/redirect-http-to-https-using-varnish/)

[Redirect HTTP to HTTPS using Varnish 4.1](https://serverfault.com/questions/835887/redirect-http-to-https-using-varnish-4-1)

[Can I force traffic to HTTPS, if so, how?](https://community.section.io/t/can-i-force-traffic-to-https-if-so-how/117)

### Caution & Troubleshooting

Varnish does not work with HTTP Basic Authentication. So If you need make it work under HTTP Basic Authentication, please refer to the articles below. It's worth trying to set HTTP authentication via Varnish.

[Varnish does not work correctly in environments with basic authentication](https://www.drupal.org/project/acquia_purge/issues/2642458)

[Varnish HTTP authentication](https://blog.tenya.me/blog/2011/12/14/varnish-http-authentication/)

## Varnish SSL Termination via Nginx (Nginx-Varnish-Apache)

[Drupal with Varnish caching and HTTPS using Nginx and Let's Encrypt](https://agile.coop/blog/drupal-with-varnish-caching-https-using-nginx-lets-encrypt/)

## Lando Development Environment

### Lando file

Refer to this [gist](https://gist.github.com/kburakozdemir/f7bbda2653164cc86ccb0b78ffcdf7f2).

<script src="https://gist.github.com/kburakozdemir/f7bbda2653164cc86ccb0b78ffcdf7f2.js"></script>

### Varnish Configuration File for Lando

> This file is different from the file on production server.

Refer to this [gist](https://gist.github.com/kburakozdemir/800fe4fa1ba3ee77a93a46d59d4dafd7).

<script src="https://gist.github.com/kburakozdemir/800fe4fa1ba3ee77a93a46d59d4dafd7.js"></script>

Be carefull this file is different from the server one (below).

## Look to lots of attached files

[default.vcl](/uploads/927ef3eb1cdd9206074f98320382c18a/default.vcl)

[https-proxy](/uploads/9596ecda0c8ae30289bfe6e19876865f/https-proxy)

[ports.conf](/uploads/21f2a994e6740d3ec3876c4906f61bc0/ports.conf)

[sayfaevi.org.conf](/uploads/bff01ca706a2b8492f93be12a0a461a4/sayfaevi.org.conf)

[security.conf](/uploads/b6fb933eb20c92a15d5d7266d5e71584/security.conf)

[notes.md](/uploads/10d4dea207614458f84fb82baf38ccd2/notes.md)

## 30 September 2020 Apache SSL Termination Version

> - [x] http --> https must be handles within varnish config
> - [ ] HTTP Basic Authentication must not be used or must be handles within varnish config
> - [ ] www --> non-www / non-www --> www operations must be handles in virtual host config or in .htaccess.

Articles used:

**Portlar için:**

[How To Use Apache2 for SSL Termination With Varnish](https://bash-prompt.net/guides/apache-varnish/)

**VHost directiflerin bazıları için:**

[Varnish and HTTPS with Apache](http://davidbu.ch/mann/blog/2015-03-20/varnish-and-https-apache.html)

**Http to https için:**

[Can I force traffic to HTTPS, if so, how?](https://community.section.io/t/can-i-force-traffic-to-https-if-so-how/117)

[Varnish 4.x to force HTTPS](https://gist.github.com/section-io-gists/2eb0f267a08734f92003f06d295af22a)

### Files for 30 September

[default.vcl](/uploads/65dfa788e0cebe75aae1186d099a3405/default.vcl)

[default.vcl.advvarnish](/uploads/0f57c70e55440f15d906625ab6bd61ab/default.vcl.advvarnish)

[default_drupal.vcl](/uploads/4dc805ab3762d28127acaee2f13a532d/default_drupal.vcl)

[external-https.conf](/uploads/511376e05dde776806f5ad41a842123a/external-https.conf)

[internal-http.conf](/uploads/d748d7961675784c060477fa8963d317/internal-http.conf)

[ports.conf](/uploads/3444fd8070dec376ff5b9c0260eb3720/ports.conf)

[settings.php](/uploads/444705ab6a1c0b51528da9ddd356ddbf/settings.php)

[varnish.service](/uploads/909d07d2a537d91be176c6ce7fa91910/varnish.service)

![image](/uploads/d867617c9cbe8dc02c3d56b3e2bb68be/image.png)

### Drupal Modules for Varnish

[Purge](https://www.drupal.org/project/purge)
[Varnish purger](https://www.drupal.org/project/varnish_purge)

#### Module Configuration References

[Drupal 8 - Varnish and Purge configuration](https://dev.docs.agile.coop/docs/processes/varnish-purge/)

[Purge cache-tags with Varnish](https://digitalist-tech.se/blog/purge-cache-tags-varnish)

[Varnish Purge 8.x-1.4 is out](https://digitalist-tech.se/blog/varnish-purge-8x-14-out)

**Important!** [Varnish (BAN) + Drupal 8](https://medium.com/@gopagoninarsinggoud/varnish-ban-drupal-8-74ca20e74f89)

#### Working config for varnish purger

![Working config for varnish purger](images/working-config-for-varnish-purger-01.png "Working config for varnish purger")

![Working config for varnish purger](images/working-config-for-varnish-purger-02.png)
