function addTargetBlank() {
  // a elements that have a href beginning with http...
  var links = document.querySelectorAll("a[href^='http']");
  var len = links.length;

  for (var i = 0; i < len; i++) {
    links[i].target = "_blank";
  }
}

function addClass(tag, classToAdd) {
  var tags = document.getElementsByTagName(tag);
  var i;
  var classes;

  for (i = 0; i < tags.length; i++) {
    classes = tags[i].className + " " + classToAdd;
    tags[i].className = classes.trim();
  }
}

window.onload = function () {
  addTargetBlank();

  addClass("img", "gallery");

  var lightboxDescription = GLightbox({
    selector: 'gallery',
  });

  addBackToTop({
    diameter: 56,
    backgroundColor: 'rgb(63, 81, 181)',
    textColor: '#fff'
  })

  var links = document.getElementsByClassName("md-nav__link--active");
  var tablinks = document.getElementsByClassName("md-tabs__link--active");
  tablinks[0].innerHTML = links[0].innerHTML;
}