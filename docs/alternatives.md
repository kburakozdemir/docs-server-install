# Alternatives

> None of those have been tested yet! Those are subject to review and test!

## Server

### Unattented Upgrades

[Using the "unattended-upgrades" package](https://help.ubuntu.com/community/AutomaticSecurityUpdates?_ga=2.185569578.2032235922.1602321025-294002190.1601662622#Using_the_.22unattended-upgrades.22_package)

[How to set up automatic updates on Ubuntu Server 18.04](https://libre-software.net/ubuntu-automatic-updates/)

```bash
nano /etc/apt/apt.conf.d/50unattended-upgrades
nano /etc/apt/apt.conf.d/20auto-upgrades
```

## E-mail

The part "Setting Up Sendmail with SSMTP Package" in [this](https://blog.edmdesigner.com/send-email-from-linux-command-line/) tutorial.

[How to Set up Postfix SMTP Relay on Ubuntu with Mailjet](https://www.linuxbabe.com/mail-server/postfix-smtp-relay)

## fail2ban with ufw

Tutorial used: [Securing Ubuntu 18.04 ssh server with ufw and fail2ban](http://blog.zedyeung.com/2018/08/14/Securing-Ubuntu-18-04-ssh-server-with-ufw-and-fail2ban/)

## Apache

### mod_security

[How to Setup ModSecurity for Apache on Ubuntu 18.04](https://hostadvice.com/how-to/how-to-setup-modsecurity-for-apache-on-ubuntu-18-04/)

[Setting Up ModSecurity on Ubuntu](https://yashagarwal.in/posts/2019/07/setting-up-modsecurity-on-ubuntu/)

### mod_evasive

[How to Install and Configure ModEvasive with Apache on Ubuntu 18.04](https://www.atlantic.net/vps-hosting/how-to-install-and-configure-modevasive-with-apache-on-ubuntu-18-04/)

### Custom Error Documents for Apache

## Hide PHP Version

Not sure about this. Hidden by default?

```bash
sudo nano /etc/php/7.2/apache2/php.ini
```

## Security

### Disable root password login

[How to Set Up SSH Keys on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804) [Step 4 — Disable Password Authentication on your Server](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-1804#step-4-%E2%80%94-disable-password-authentication-on-your-server)

### Checklist

Old but an interesting list

[Best practices for hardening new sever in 2017](https://www.digitalocean.com/community/questions/best-practices-for-hardening-new-sever-in-2017)

## Testing

### Apache Bench Tutorial

Load testing with Apache Benchmak. [Apache Bench Tutorial](https://www.tutorialspoint.com/apache_bench/index.htm)
