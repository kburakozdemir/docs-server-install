# E-mail Configuration

In order for applications to send e-mails from the server, there must be some MTA installed.

If an MTA is installed, there is no need for SMTP module for Drupal.

## sendmail

Zero config option is `sendmail`.

> This is tested with Drupal.
> If `sendmail` is installed, Drupal can send e-mails.

```bash
sudo apt install sendmail
```

### Test 1

```bash
echo "Subject: sendmail test" | sendmail -v my@email.com
```

### Test 2

mail.txt file

```txt
To: my@email.com
Subject: sendmail test two
From: me@myserver.com

And here goes the e-mail body, test test test..
```

```bash
sendmail -vt < mail.txt
```

## Postfix

### GSuite, Gmail SMTP Configured

In some cases, if usage of SMTP needed for MTA, use `postfix`.

The tutorial to use is [Configure Postfix to Use Gmail SMTP on Ubuntu 18.04](https://kifarunix.com/configure-postfix-to-use-gmail-smtp-on-ubuntu-18-04/).

> Important!

2 pain points are these:

1. `myhostnme` and `mydestination` variables in `postfix` config
2. `From` name in e-mail

If `myhostnme` and `mydestination` variables are not configured correctly, system cannot send mail to the same domain while being able to send to other domain.

Refer to [Install and Configure Postfix with Gmail SMTP for Perfect Mailing System](https://restorebin.com/configure-postfix-smtp-relay/) to resolve these 2 issues.

```shell
apt install libsasl2-modules mailutils postfix
```

### Test

```bash
# One-liner
echo "Test Postfix Gmail Relay" | \
  mail -s "sunucunun içinden Postfix Gmail Relay" \
  your-email@address.com
```

## Troubleshooting

### Slow sendmail Process

Check your hostname. Look at [this](https://www.digitalocean.com/community/questions/sendmail-is-slow-to-send-mail).

Resolution for Digital Ocean is here [How do i change hostname?](https://www.digitalocean.com/community/questions/how-do-i-change-hostname).
