# Composer & Drush

## Install Composer

[How To Install and Use Composer on Ubuntu 22.04](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-composer-on-ubuntu-22-04)

## Install Drush Launcher

[Drush Launcher](https://github.com/drush-ops/drush-launcher)

## Install Drush Globally

```bash
# One-liner
sudo ln -s \
  /usr/local/bin/composer \
  /usr/bin/composer

# One-liner
sudo git clone \
  https://github.com/drush-ops/drush.git \
  /usr/local/src/drush

cd /usr/local/src/drush

# View the GitHub project’s
# releases page to view all available versions.
# https://github.com/drush-ops/drush/releases
sudo git checkout 8.4.5

# One-liner
sudo ln -s \
  /usr/local/src/drush/drush \
  /usr/bin/drush

sudo composer install

# Check Version
drush --version
```

## Reference

[Install Drush on Ubuntu 18.04](https://www.linode.com/docs/websites/cms/drupal/how-to-install-drush-on-ubuntu-18-04/)
