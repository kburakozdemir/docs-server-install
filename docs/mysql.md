# MySQL Installation

## Install

```bash
sudo apt install mysql-server -y
```

## MySQL Security

```bash
sudo mysql_secure_installation
```

> Disable `auth_socket` for `root` user. [See](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04#step-3-%E2%80%94-(optional)-adjusting-user-authentication-and-privileges).

## MySQL 5.7.31 bug

> Very important! Do this after creating the database.

```bash
# Check your version
mysql --version
```

```sql
GRANT USAGE ON *.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuser_password';
GRANT SELECT ON *.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuser_password';
GRANT PROCESS ON *.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuser_password';
SHOW GRANTS for dbuser@localhost;
```

## Reference

[How To Install MySQL on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-18-04)

## Troubleshooting (Temporary! This will be removed.)

## Optimize MySQL

[How to Optimize MySQL Performance Using MySQLTuner](https://www.linode.com/docs/databases/mysql/how-to-optimize-mysql-performance-using-mysqltuner/)

## Remove and reinstall MySQL

```bash
# Turn on maintenance mode
drush --root=/path/to/your/drupal/docroot sset system.maintenance_mode TRUE

# Dump sql
drush sql-dump --root=/path/to/your/drupal/docroot | gzip > dbname_$(date +%Y%m%d_%H%M%S).sql.gz

sudo service mysql stop
sudo apt-get remove --purge mysql*
sudo apt-get purge mysql*
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt-get remove dbconfig-mysql

# Install MySQL again here

mysql -u root -p
CREATE DATABASE dbname CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON dbname.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuserpass';

GRANT USAGE ON *.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuserpass';
GRANT SELECT ON *.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuserpass';
GRANT PROCESS ON *.* TO 'dbuser'@'localhost' IDENTIFIED BY 'dbuserpass';
SHOW GRANTS for dbuser@localhost;

mysql -u root -p dbname < /path/to/dump/file.sql
# Turn off maintenance mode
drush --root=/path/to/your/drupal/docroot sset system.maintenance_mode TRUE
```
